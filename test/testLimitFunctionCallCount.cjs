const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function testFunction() {
    return "I am callback Function";
}

const limitedFunction = limitFunctionCallCount(testFunction, 3);
let count = 4;
while (count > 0) {
    console.log(limitedFunction());
    count--;
}


