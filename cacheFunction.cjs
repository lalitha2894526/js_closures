function cacheFunction(cb) {
    try{
    const cache = {};

    return function () {
        const key = 'uniqueKey';

        if (cache[key]) {
            return cache[key];
        } else {
            const result = cb();
            cache[key] = result;
            return result;
        }
    }
}
catch(error){
    console.log(error.message);
}
    
}

module.exports = cacheFunction;
