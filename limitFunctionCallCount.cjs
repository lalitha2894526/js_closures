function limitFunctionCallCount(callback, n) {
    try {
        let callCount = 0;

        function test() {
            if (callCount < n) {
                callCount++;
                return callback();
            } else {
                return null;
            }
        }
        return test;
    }
    catch (error) {
        console.log(error.message);
    }
}

module.exports = limitFunctionCallCount;
